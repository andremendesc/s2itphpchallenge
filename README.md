challenge
=========

PHP Challenge - S2it

Done:

	-docker
	-symfony2
	-composer
	-mysql
	-index page with drag & drop feature for file imports
	-database schema ORMized

Todo:

	On UploadController:
		-data validation
		-data persistence
	-generated docs
	-unit/funcional tests

How to run:

	git clone git@bitbucket.org:andremendesc/s2itphpchallenge.git
	cd s2itphpchallenge
	#docker-compose build
	#docker-compose up -d
	
	#docker exec -t -i `docker ps --quiet --filter "name=challenge"` bash
	
	Or alternatively, with sudo:
	
	$sudo docker exec -t -i `sudo docker ps --quiet --filter "name=challenge"` bash
	
	then:

	$cd challenge
	#composer install
	#php app/console doctrine:database:create;
	#php app/console doctrine:schema:create;
	#chmod 777 web/uploads

then go to https://localhost:8080 and drag files in