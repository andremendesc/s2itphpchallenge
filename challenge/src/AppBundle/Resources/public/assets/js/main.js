        'use strict';

        ;( function( $, window, document, undefined )
        {
            var isAdvancedUpload = function(){
                var div = document.createElement( 'div' );
                return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
            }();

            $('.proccessBtnDiv').each(function() {
                var $formProccessXml        = $( this );
                var ajaxData = new FormData( $formProccessXml.get( 0 ) );
                $formProccessXml.on( 'submit', function( e ) {
                    e.preventDefault();
                    $.ajax(
                    {
                        url:            $formProccessXml.attr( 'action' ),
                        type:           $formProccessXml.attr( 'method' ),
                        data:           ajaxData,
                        dataType:       'json',
                        cache:          false,
                        contentType:    false,
                        processData:    false,
                        complete: function( data )
                        {
                            console.dir(data); 
                        },
                        success: function( data )
                        {
                            console.dir(data);
                        },
                        error: function()
                        {
                            alert( 'Error. Please, contact the webmaster!' );
                        }
                    });
                });
            });

            $( '.box' ).each( function()
            {
                var $form        = $( this ),
                    $input       = $form.find( 'input[type="file"]' ),
                    $label       = $form.find( 'label' ),
                    $errorMsg    = $form.find( '.box__error span' ),
                    $restart     = $form.find( '.box__restart' ),
                    droppedFiles = false,
                    showFiles    = function( files )
                    {
                        $label.text( files.length > 1 ? ( $input.attr( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name );
                    };

                $form.append( '<input type="hidden" name="ajax" value="1" />' );

                $input.on( 'change', function( e )
                {
                    showFiles( e.target.files );
                    $form.trigger( 'submit' );
                });

                if( isAdvancedUpload )
                {
                    $form
                    .addClass( 'has-advanced-upload' ) 
                    .on( 'drag dragstart dragend dragover dragenter dragleave drop', function( e )
                    {
                        e.preventDefault();
                        e.stopPropagation();
                    })
                    .on( 'dragover dragenter', function() 
                    {
                        $form.addClass( 'is-dragover' );
                    })
                    .on( 'dragleave dragend drop', function()
                    {
                        $form.removeClass( 'is-dragover' );
                    })
                    .on( 'drop', function( e )
                    {
                        droppedFiles = e.originalEvent.dataTransfer.files; 
                        showFiles( droppedFiles );
                        $form.trigger( 'submit' ); 
                        
                    });
                }

                $form.on( 'submit', function( e )
                {
                    if( $form.hasClass( 'is-uploading' ) ) return false;
                    $form.addClass( 'is-uploading' ).removeClass( 'is-error' );
                    if( isAdvancedUpload )
                    {
                        e.preventDefault();

                        var ajaxData = new FormData( $form.get( 0 ) );
                        if( droppedFiles )
                        {
                            $.each( droppedFiles, function( i, file )
                            {
                                ajaxData.append( $input.attr( 'name' ), file );
                            });
                        }

                        $.ajax(
                        {
                            url:            $form.attr( 'action' ),
                            type:           $form.attr( 'method' ),
                            data:           ajaxData,
                            dataType:       'json',
                            cache:          false,
                            contentType:    false,
                            processData:    false,
                            complete: function()
                            {
                                $form.removeClass( 'is-uploading' );
                            },
                            success: function( data )
                            {
                                $form.addClass( data.success == true ? 'is-success' : 'is-error' );
                                if( !data.success ) $errorMsg.text( data.error );
                                //Se foi sucesso, mostrar o botão de processar
                                $('.proccessBtnDiv').removeClass('hidden');
                                $('input[name="xmlFileName"]').val(data.xmlFileName);
                                console.dir(data.xmlFileName);
                                if( !data.success ) $errorMsg.text( 'Erro' );
                            },
                            error: function()
                            {
                                alert( 'Error. Please, contact the webmaster!' );
                            }
                        });
                    } else {
                        var iframeName  = 'uploadiframe' + new Date().getTime(),
                            $iframe     = $( '<iframe name="' + iframeName + '" style="display: none;"></iframe>' );

                        $( 'body' ).append( $iframe );
                        $form.attr( 'target', iframeName );

                        $iframe.one( 'load', function()
                        {
                            var data = $.parseJSON( $iframe.contents().find( 'body' ).text() );
                            $form.removeClass( 'is-uploading' ).addClass( data.success == true ? 'is-success' : 'is-error' ).removeAttr( 'target' );
                            if( !data.success ) $errorMsg.text( data.error );
                            $iframe.remove();
                        });
                    }
                });

                $restart.on( 'click', function( e )
                {
                    e.preventDefault();
                    $form.removeClass( 'is-error is-success' );
                    $input.trigger( 'click' );
                });

                $input
                .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });

        })( jQuery, window, document );
