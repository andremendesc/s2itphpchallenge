<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShipOrder
 *
 * @ORM\Table(name="ship_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShipOrderRepository")
 */
class ShipOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="orders")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="ShipAddress", inversedBy="orders")
     * @ORM\JoinColumn(name="shipaddress_id", referencedColumnName="id")
     */
    private $shipAddress;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="orders")
     * @ORM\JoinColumn(name="items_id", referencedColumnName="id")
     */
    private $items;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     *
     * @return ShipOrder
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set shipAddress
     *
     * @param \AppBundle\Entity\ShipAddress $shipAddress
     *
     * @return ShipOrder
     */
    public function setShipAddress(\AppBundle\Entity\ShipAddress $shipAddress = null)
    {
        $this->shipAddress = $shipAddress;

        return $this;
    }

    /**
     * Get shipAddress
     *
     * @return \AppBundle\Entity\ShipAddress
     */
    public function getShipAddress()
    {
        return $this->shipAddress;
    }

    /**
     * Set items
     *
     * @param \AppBundle\Entity\items $items
     *
     * @return ShipOrder
     */
    public function setItems(\AppBundle\Entity\Item $items = null)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * Get items
     *
     * @return \AppBundle\Entity\items
     */
    public function getItems()
    {
        return $this->items;
    }
}
