<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var phones
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Phone", inversedBy="persons")
     * @ORM\JoinColumn(name="phone_id", referencedColumnName="id")
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="ShipOrder", mappedBy="person")
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Person
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phones
     *
     * @param \AppBundle\Entity\Phone $phones
     *
     * @return Person
     */
    public function setPhones(\AppBundle\Entity\Phone $phones = null)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return \AppBundle\Entity\Phone
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\ShipOrder $order
     *
     * @return Person
     */
    public function addOrder(\AppBundle\Entity\ShipOrder $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\ShipOrder $order
     */
    public function removeOrder(\AppBundle\Entity\ShipOrder $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
