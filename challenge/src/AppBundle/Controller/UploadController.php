<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Util\XmlUtils;
use AppBundle\Entity\XmlFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class UploadController extends Controller
{
    /**
     * @Route("/upload/new", name="app_product_new")
     */
    public function newAction(Request $request)
    {
        $product = new XmlFile();
        if ($this->getRequest()->isMethod('POST')) {

            $params = $this->getRequest()->request->all();

            if( $this->getRequest()->request->get('proccess') == 1 ) {
                // Due some yet unknown reason, xmlFileName is coming empty T.T
                return new JsonResponse(array(
                    'success' => true,
                    'proccess' => true,
                    'xmlFileName' => $this->getRequest()->request->get('xmlFileName')
                ));
            }

            foreach($request->files as $uploadedFile) {
                
                $xmlFilePath =  $this->container->getParameter('kernel.root_dir') 
                                .
                                "/../web/uploads";

                $xmlFileName = md5(uniqid()).'.xml';

                $file = $uploadedFile->move($xmlFilePath, $xmlFileName);

                try {

                    $document = new \SimpleXMLElement(
                                        file_get_contents($xmlFilePath.'/'.$xmlFileName)
                                    );
                } catch (\Exception $e) {

                    return new JsonResponse(array(
                        'success' => false
                    ));

                }

                // So it's a valid XML file. Does it contain data we can receive?

                $canProccess = false;

                if( key($document) == 'shiporder' ) {
                    foreach ($document->shiporder as $order) {
                        $orderExists = $this->getDoctrine()
                            ->getRepository(\AppBundle\Entity\ShipOrder::class)
                            ->find($order->orderid);
                        if ($orderExists === null) {
                            $canProccess = true;
                        }
                    }
                }

                if( key($document) == 'person' ) {
                    foreach ($document->person as $person) {
                        $personExists = $this->getDoctrine()
                            ->getRepository(\AppBundle\Entity\Person::class)
                            ->find($person->personid);
                        if ($personExists === null) {
                            $canProccess = true;
                        }
                    }
                }
            }

            // $product->setName($fileName);

            return new JsonResponse(array(
                'success' => true,
                'proccess' => $canProccess,
                'xmlFileName' => $xmlFileName
            ));
        } else {
            return $this->render('default/index.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            ));
        }
    }
}
