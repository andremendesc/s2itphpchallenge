#!/bin/bash
mkdir -p challenge/app/cache challenge/app/logs
touch challenge/app/logs/prod.log
touch challenge/app/logs/dev.log
chgrp -R www-data .
chmod -R g+w challenge/app/cache challenge/app/logs
source /etc/apache2/envvars
tail -F /var/log/apache2/* challenge/app/logs/prod.log challenge/app/logs/dev.log &
mkdir challenge/web/uploads
chmod 777 -R challenge/web/uploads
exec apache2 -D FOREGROUND
